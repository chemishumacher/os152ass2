// Shell.

#include "types.h"
#include "user.h"
#include "fcntl.h"

// Parsed command representation
#define EXEC  1
#define REDIR 2
#define PIPE  3
#define LIST  4
#define BACK  5

#define MAXARGS 10
#define BUFFSIZE 100

struct process {
  int pid;
  char procName[BUFFSIZE];
  struct process *next;
};

struct job {
  char jobName[BUFFSIZE];
  int index;
  struct process *procList;
  struct job *next;
};

struct cmd {
  int type;
};

struct execcmd {
  int type;
  char *argv[MAXARGS];
  char *eargv[MAXARGS];
};

struct redircmd {
  int type;
  struct cmd *cmd;
  char *file;
  char *efile;
  int mode;
  int fd;
};

struct pipecmd {
  int type;
  struct cmd *left;
  struct cmd *right;// create pipe with child
};

struct listcmd {
  int type;
  struct cmd *left;
  struct cmd *right;
};

struct backcmd {
  int type;
  struct cmd *cmd;
};

int fork1(void); 	// Fork but panics on failure.
int pipe1(int*);	// Pipe but panics on failure.
void panic(char*);
struct cmd *parsecmd(char*);

// job functions
struct job * createJob(char*);
struct process * createProcess(char*, int);
void refreshJobList(struct job*);
void insertJobToList(struct job*, struct job*);
void insertProcessToJob(struct job*, struct process*);
void printJobs(struct job*);
void fg(struct job*, int);

// Execute cmd.  Never returns.
void
runcmd(struct cmd *cmd, int *papaPipe)
{
  int p[2], p1[2], p2[2];
  struct backcmd *bcmd;
  struct execcmd *ecmd;
  struct listcmd *lcmd;
  struct pipecmd *pcmd;
  struct redircmd *rcmd;
  struct process *newProc;

  if(cmd == 0)
  {
	  close(papaPipe[1]);
	  exit(0);
  }

  switch(cmd->type){
  default:
    panic("runcmd");
    break;

  case EXEC:
    ecmd = (struct execcmd*)cmd;
    if(ecmd->argv[0] == 0)
    {
  	  close(papaPipe[1]);
      exit(0);
    }

    newProc = createProcess(ecmd->argv[0], getpid());
    write(papaPipe[1], newProc, sizeof(struct process));
    close(papaPipe[1]);		// must sends EOF to papa
    exec(ecmd->argv[0], ecmd->argv);
    printf(2, "exec %s failed\n", ecmd->argv[0]);
    break;

  case REDIR:
    rcmd = (struct redircmd*)cmd;
    close(rcmd->fd);
    if(open(rcmd->file, rcmd->mode) < 0){
      printf(2, "open %s failed\n", rcmd->file);
      exit(0);
    }
    runcmd(rcmd->cmd, papaPipe);
    break;

  case LIST:
    lcmd = (struct listcmd*)cmd;
    pipe1(p1);
    if(fork1() == 0)
    {
    	close(p1[0]);			// close pipe read
    	close(papaPipe[1]); 	// close copy of unneeded papaPipe
    	runcmd(lcmd->left, p1);
    }
    close(p1[1]);		// close pipe write
    newProc = (struct process*)malloc(sizeof(struct process));
	while (read(p1[0], newProc, sizeof(struct process)) > 0)
		write(papaPipe[1], newProc, sizeof(struct process));
	close(p1[0]);

    wait(0);
    runcmd(lcmd->right, papaPipe);
    break;

  case PIPE:
	  printf(1,"pipiepeepep");
    pcmd = (struct pipecmd*)cmd;
    pipe1(p);
    pipe1(p1);
    if(fork1() == 0){
      close(1);		// close standard output
      dup(p[1]);	// assign p[1] as standard output 1
      close(p[0]);	// close the pipes
      close(p[1]);	// close the pipes

      close(p1[0]);			// close pipe read
      close(papaPipe[1]);	// close copy of unneeded papaPipe
      runcmd(pcmd->left, p1);
    }
    pipe(p2);
    if(fork1() == 0){
      close(0);
      dup(p[0]);
      close(p[0]);
      close(p[1]);

      close(p2[0]);			// close pipe read
      close(p1[0]);			// close p1 pipe. we dont need it
      close(p1[1]);			// close p1 pipe. we dont need it
      close(papaPipe[1]);	// close copy of unneeded papaPipe
      runcmd(pcmd->right, p2);
    }
    // papa don't need this pipe. this pipe is for the children above.
    close(p[0]);
    close(p[1]);
    // papa don't need the write pipes
    close(p1[1]);
    close(p2[1]);

    newProc = (struct process*)malloc(sizeof(struct process));
    // read process from the first child
    while (read(p1[0], newProc, sizeof(struct process)) > 0)
    	write(papaPipe[1], newProc, sizeof(struct process));
    close(p1[0]);

    // read process from the second child
    while (read(p2[0], newProc, sizeof(struct process)) > 0)
    	write(papaPipe[1], newProc, sizeof(struct process));
    close(p2[0]);

    close(papaPipe[1]);	// send EOF to father
    wait(0);
    wait(0);
    break;
    
  case BACK:
    bcmd = (struct backcmd*)cmd;
    if (fork1() == 0)
    	runcmd(bcmd->cmd, papaPipe);

    close(papaPipe[1]); // there is another papaPipe copy opened in the child above. so close one unneeded end.
    break;
  }
  printf(1, "pid: %d exiting\n", getpid());
  exit(0);
}

int
getcmd(char *buf, int nbuf)
{
  printf(2, "$ ");
  memset(buf, 0, nbuf);
  gets(buf, nbuf);
  if(buf[0] == 0) // EOF
    return -1;
  return 0;
}

int
main(void)
{
  static char buf[BUFFSIZE];
  struct job *jobList = (struct job*)malloc(sizeof(struct job));
  struct job *newJob = 0;
  struct process *newProc = 0;
  int fd, i, j, chk, pid, papaPipe[2];

  // init the first job = represents an empty list
  jobList->index = -1;
  jobList->next = 0;

  // Assumes three file descriptors open.
  while((fd = open("console", O_RDWR)) >= 0){
    if(fd >= 3){
      close(fd);
      break;
    }
  }
  
  // Read and run input commands.
  while(getcmd(buf, sizeof(buf)) >= 0)
  {
	  refreshJobList(jobList);

	  // cd command
	  if(buf[0] == 'c' && buf[1] == 'd' && buf[2] == ' ')
	  {
		  // Clumsy but will have to do for now.
		  // Chdir has no effect on the parent if run in the child.
		  buf[strlen(buf)-1] = 0;  // chop \n
		  if(chdir(buf+3) < 0)
			printf(2, "cannot cd %s\n", buf+3);
		  continue;
	  }

	  // jobs command
	  if (buf[0] == 'j' && buf[1] == 'o' && buf[2] == 'b' && buf[3] == 's')
	  {
		  chk = 1;
		  for (i = 4; i < strlen(buf)-1; i++)
			  if (buf[i] != ' ')
			  {
				  chk = 0;
				  break;
			  }

		  if (chk)  // if good syntax
			  printJobs(jobList);
		  else
			  printf(2, "bad jobs syntax\n");

		  continue;
	  }

	  // fg command
	  if (buf[0] == 'f' && buf[1] == 'g')
	  {
		  if (buf[2] == '\n')
		  {
			  printf(1, " empty");
			  fg(jobList, 0);
			  continue;
		  }

		  buf[strlen(buf)-1] = 0;  // chop \n
		  if (buf[2] == ' ')
		  {
			  chk = 1;
			  // loop the string to find the first digit.
			  for (i = 3; i < strlen(buf); i++)
			  {
				  if ('0' <= buf[i] && buf[i] <= '9')
					  break;
				  else
					  if (buf[i] == ' ')
						  continue;

				  chk = 0;
				  break;
			  }

			  if (i == strlen(buf))
			  { // if true, then all spaces
				  fg(jobList, 0);
				  continue;
			  }

			  if (chk)
			  { // if true then we stopped because we found a digit.

				  // loop the rest of the string to find NOT a digit
				  for (j = i; j < strlen(buf); j++)
				  {
					  if ('0' <= buf[j] && buf[j] <= '9')
						  continue;
					  else
						  if (buf[j] == ' ')
							  break;

					  chk = 0;
					  break;
				  }

				  if (j == strlen(buf))
				  { // the rest of the string are digits
					  fg(jobList, atoi(buf+i));
					  continue;
				  }


				  if (chk)
				  { // if true then we stopped because we found a space after a digit

					  // loop to see if all the rest are space
					  for (; j < strlen(buf); j++)
						  if (buf[j] != ' ' )
							  break;

					  if (j == strlen(buf))
					  {	// if true, then all the rest of the string are spaces
						  fg(jobList, atoi(buf+i));
						  continue;
					  }
					  else
					  {
						  printf(2, "cannot fg %s\n", buf+2);
						  continue;
					  }
				  }
				  else
				  { // then we stopped because we found not a digit after a digit
					  printf(2, "cannot fg %s\n", buf+2);
					  continue;
				  }
			  }
			  else
			  {
				  printf(2, "cannot fg %s\n", buf+2);
				  continue;
			  }
		  }
	  }

    // create pipe with child
    pipe1(papaPipe);
    // create a new job
    newJob = createJob(buf);

    // create child to run the process.
    pid = fork1();
	if(pid == 0)
	{ // child
		close(papaPipe[0]);			// close pipe read
	    runcmd(parsecmd(buf), papaPipe);
	}
	else
	{ // papa
		close(papaPipe[1]);			// close pipe write
		newProc = (struct process*)malloc(sizeof(struct process));
		while (read(papaPipe[0], newProc, sizeof(struct process)) > 0)
		{
			insertProcessToJob(newJob, newProc);
			newProc = (struct process*)malloc(sizeof(struct process));
		}
		close(papaPipe[0]);
		insertJobToList(jobList, newJob);
	}
    wait(0);
  }
  exit(0);
}

void
panic(char *s)
{
  printf(2, "%s\n", s);
  exit(0);
}

int
pipe1(int *p)
{
	int ret;

	ret = pipe(p);
	if (ret)
		panic("Pipe failed.\n");
	return ret;
}

int
fork1(void)
{
  int pid;
  
  pid = fork();
  if(pid == -1)
    panic("fork");
  return pid;
}

//PAGEBREAK!
// Constructors

struct cmd*
execcmd(void)
{
  struct execcmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = EXEC;
  return (struct cmd*)cmd;
}

struct cmd*
redircmd(struct cmd *subcmd, char *file, char *efile, int mode, int fd)
{
  struct redircmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = REDIR;
  cmd->cmd = subcmd;
  cmd->file = file;
  cmd->efile = efile;
  cmd->mode = mode;
  cmd->fd = fd;
  return (struct cmd*)cmd;
}

struct cmd*
pipecmd(struct cmd *left, struct cmd *right)
{
  struct pipecmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = PIPE;
  cmd->left = left;
  cmd->right = right;
  return (struct cmd*)cmd;
}

struct cmd*
listcmd(struct cmd *left, struct cmd *right)
{
  struct listcmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = LIST;
  cmd->left = left;
  cmd->right = right;
  return (struct cmd*)cmd;
}

struct cmd*
backcmd(struct cmd *subcmd)
{
  struct backcmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = BACK;
  cmd->cmd = subcmd;
  return (struct cmd*)cmd;
}
//PAGEBREAK!
// Parsing

char whitespace[] = " \t\r\n\v";
char symbols[] = "<|>&;()";

int
gettoken(char **ps, char *es, char **q, char **eq)
{
  char *s;
  int ret;
  
  s = *ps;
  while(s < es && strchr(whitespace, *s))
    s++;
  if(q)
    *q = s;
  ret = *s;
  switch(*s){
  case 0:
    break;
  case '|':
  case '(':
  case ')':
  case ';':
  case '&':
  case '<':
    s++;
    break;
  case '>':
    s++;
    if(*s == '>'){
      ret = '+';
      s++;
    }
    break;
  default:
    ret = 'a';
    while(s < es && !strchr(whitespace, *s) && !strchr(symbols, *s))
      s++;
    break;
  }
  if(eq)
    *eq = s;
  
  while(s < es && strchr(whitespace, *s))
    s++;
  *ps = s;
  return ret;
}

int
peek(char **ps, char *es, char *toks)
{
  char *s;
  
  s = *ps;
  while(s < es && strchr(whitespace, *s))
    s++;
  *ps = s;
  return *s && strchr(toks, *s);
}

struct cmd *parseline(char**, char*);
struct cmd *parsepipe(char**, char*);
struct cmd *parseexec(char**, char*);
struct cmd *nulterminate(struct cmd*);

struct cmd*
parsecmd(char *s)
{
  char *es;
  struct cmd *cmd;

  es = s + strlen(s);
  cmd = parseline(&s, es);
  peek(&s, es, "");
  if(s != es){
    printf(2, "leftovers: %s\n", s);
    panic("syntax");
  }
  nulterminate(cmd);
  return cmd;
}

struct cmd*
parseline(char **ps, char *es)
{
  struct cmd *cmd;

  cmd = parsepipe(ps, es);
  while(peek(ps, es, "&")){
    gettoken(ps, es, 0, 0);
    cmd = backcmd(cmd);
  }
  if(peek(ps, es, ";")){
    gettoken(ps, es, 0, 0);
    cmd = listcmd(cmd, parseline(ps, es));
  }
  return cmd;
}

struct cmd*
parsepipe(char **ps, char *es)
{
  struct cmd *cmd;

  cmd = parseexec(ps, es);
  if(peek(ps, es, "|")){
    gettoken(ps, es, 0, 0);
    cmd = pipecmd(cmd, parsepipe(ps, es));
  }
  return cmd;
}

struct cmd*
parseredirs(struct cmd *cmd, char **ps, char *es)
{
  int tok;
  char *q, *eq;

  while(peek(ps, es, "<>")){
    tok = gettoken(ps, es, 0, 0);
    if(gettoken(ps, es, &q, &eq) != 'a')
      panic("missing file for redirection");
    switch(tok){
    case '<':
      cmd = redircmd(cmd, q, eq, O_RDONLY, 0);
      break;
    case '>':
      cmd = redircmd(cmd, q, eq, O_WRONLY|O_CREATE, 1);
      break;
    case '+':  // >>
      cmd = redircmd(cmd, q, eq, O_WRONLY|O_CREATE, 1);
      break;
    }
  }
  return cmd;
}

struct cmd*
parseblock(char **ps, char *es)
{
  struct cmd *cmd;

  if(!peek(ps, es, "("))
    panic("parseblock");
  gettoken(ps, es, 0, 0);
  cmd = parseline(ps, es);
  if(!peek(ps, es, ")"))
    panic("syntax - missing )");
  gettoken(ps, es, 0, 0);
  cmd = parseredirs(cmd, ps, es);
  return cmd;
}

struct cmd*
parseexec(char **ps, char *es)
{
  char *q, *eq;
  int tok, argc;
  struct execcmd *cmd;
  struct cmd *ret;
  
  if(peek(ps, es, "("))
    return parseblock(ps, es);

  ret = execcmd();
  cmd = (struct execcmd*)ret;

  argc = 0;
  ret = parseredirs(ret, ps, es);
  while(!peek(ps, es, "|)&;")){
    if((tok=gettoken(ps, es, &q, &eq)) == 0)
      break;
    if(tok != 'a')
      panic("syntax");
    cmd->argv[argc] = q;
    cmd->eargv[argc] = eq;
    argc++;
    if(argc >= MAXARGS)
      panic("too many args");
    ret = parseredirs(ret, ps, es);
  }
  cmd->argv[argc] = 0;
  cmd->eargv[argc] = 0;
  return ret;
}

// NUL-terminate all the counted strings.
struct cmd*
nulterminate(struct cmd *cmd)
{
  int i;
  struct backcmd *bcmd;
  struct execcmd *ecmd;
  struct listcmd *lcmd;
  struct pipecmd *pcmd;
  struct redircmd *rcmd;

  if(cmd == 0)
    return 0;
  
  switch(cmd->type){
  case EXEC:
    ecmd = (struct execcmd*)cmd;
    for(i=0; ecmd->argv[i]; i++)
      *ecmd->eargv[i] = 0;
    break;

  case REDIR:
    rcmd = (struct redircmd*)cmd;
    nulterminate(rcmd->cmd);
    *rcmd->efile = 0;
    break;

  case PIPE:
    pcmd = (struct pipecmd*)cmd;
    nulterminate(pcmd->left);
    nulterminate(pcmd->right);
    break;
    
  case LIST:
    lcmd = (struct listcmd*)cmd;
    nulterminate(lcmd->left);
    nulterminate(lcmd->right);
    break;

  case BACK:
    bcmd = (struct backcmd*)cmd;
    nulterminate(bcmd->cmd);
    break;
  }
  return cmd;
}

//PAGEBREAK!
// Jobs

struct job * createJob(char *jobName)
{
	static int counter = 1;
	struct job *retJob = (struct job*)malloc(sizeof(struct job));

	jobName[strlen(jobName) - 1] = 0; // chop \n in the end
	strcpy(retJob->jobName, jobName);
	retJob->index = counter;
	retJob->procList = 0;
	retJob->next = 0;

	++counter;
	return retJob;
}

void printJobs(struct job *jobList)
{
  struct job *tmpJobs = jobList->next;
  struct process *tmpProcList = 0;

  // if there is no Jobs in the list
  if (tmpJobs == 0)
  {
	  printf(1, "There are no jobs\n");
	  return;
  }
  // loop all the list of jobs, and print them
  while (tmpJobs != 0)
  {
	  printf(1, "Job %d: %s\n", tmpJobs->index, tmpJobs->jobName);
	  tmpProcList = tmpJobs->procList;
	  tmpProcList = tmpProcList->next;
	  while (tmpProcList != 0)
	  {
		  printf(1, "%d: %s\n", tmpProcList->pid, tmpProcList->procName);
		  tmpProcList = tmpProcList->next;
	  }
	  tmpJobs = tmpJobs->next;
  }
}

void insertJobToList(struct job *jobList, struct job *job)
{
	struct job *tmpJob = jobList;

	while (tmpJob->next != 0)	// loop to the end of the list
		tmpJob = tmpJob->next;

	tmpJob->next = job;
}

void refreshJobList(struct job *jobList)
{
	struct job *tmpJob1 = 0, *tmpJob2 = 0;
	struct process *tmpProc1 = 0, *tmpProc2 = 0;
	int procStatus = 0;

	tmpJob1 = jobList;
	tmpJob2 = tmpJob1->next;
	while (tmpJob2 != 0)
	{
		if (tmpJob2->procList == 0)
		{
			tmpJob1->next = tmpJob2->next;
			tmpJob2 = tmpJob2->next;
			continue;
		}
		tmpProc1 = tmpJob2->procList;
		tmpProc2 = tmpProc1->next;
		while (tmpProc2 != 0)
		{
			procStatus = isalive(tmpProc2->pid);
			if (!procStatus)
			{
				tmpProc1->next = tmpProc2->next;
				tmpProc2 = tmpProc2->next;
				continue;
			}
			tmpProc1 = tmpProc2;
			tmpProc2 = tmpProc2->next;
		}

		tmpProc2 = tmpJob2->procList;
		if (tmpProc2->next == 0) // no processes
		{
			tmpJob1->next = tmpJob2->next;
			tmpJob2 = tmpJob2->next;
			continue;
		}
		tmpJob1 = tmpJob2;
		tmpJob2 = tmpJob2->next;
	}

}

void insertProcessToJob(struct job *job, struct process *proc)
{
	struct process *tmpProc = 0;
	if (job->procList == 0)
	{
		tmpProc = (struct process*)malloc(sizeof(struct process));
		tmpProc->pid = -1;	// will be the start point. -1 represents empty list
		tmpProc->next = proc;
		job->procList = tmpProc;
		return;
	}
	tmpProc = job->procList;
	while (tmpProc->next != 0)
		tmpProc = tmpProc->next;

	tmpProc->next = proc;
}

struct process * createProcess(char *procName, int pid)
{
	struct process *retProcess = (struct process*)malloc(sizeof(struct process));
	strcpy(retProcess->procName, procName);
	retProcess->pid = pid;
	retProcess->next = 0;

	return retProcess;
}

void fg(struct job *jobList, int index)
{
	struct job *tmpJob = jobList->next;
	struct process *tmpProc = 0;
	int chk = 0;

	chk = 0;
	while (tmpJob != 0)
	{
		if (index == 0)
		{
			tmpProc = tmpJob->procList;
			chk = 1;
			break;
		}
		if (tmpJob->index == index)
		{
			tmpProc = tmpJob->procList;
			chk = 1;
			break;
		}
		tmpJob = tmpJob->next;
	}

	// no job with the index
	if (!chk)
		return;

	// loop all the process in the job, and waitpid with BLOCKING till all processes end.
	tmpProc = tmpProc->next;
	while (tmpProc != 0)
	{
		waitpid(tmpProc->pid, 0 ,1);
		tmpProc = tmpProc->next;
	}
}






