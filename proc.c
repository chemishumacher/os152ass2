#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "x86.h"
#include "proc.h"
#include "spinlock.h"

struct {
  struct spinlock lock;
  struct proc proc[NPROC];
} ptable;

static struct proc *initproc;

extern void forkret(void);
extern void trapret(void);

static void wakeup1(void*);
static void updateStat1(void);

void
pinit(void)
{
  initlock(&ptable.lock, "ptable");
}

void updateState(struct proc *p, int state)
{
	switch (state)
	{	// we do p->state = state in each case, because maybe we want to do things before changing the state
		case UNUSED:
			(p->pstime).begin_retime = 0xFFFFFFFF;
			p->state = state;
			break;
		case EMBRYO:
			(p->pstime).stime = 0;
			(p->pstime).retime = 0;
			(p->pstime).rutime = 0;
			(p->pstime).cfs_rutime = 0;
			(p->pstime).ctime = ticks;
			(p->pstime).ttime = -1;
			(p->pstime).begin_retime = 0xFFFFFFFF;
			p->state = state;
			break;
		case SLEEPING:
			p->state = state;
			break;
		case RUNNABLE:
			(p->pstime).begin_retime = ticks;
			p->state = state;
			break;
		case RUNNING:
			(p->pstime).begin_retime = 0xFFFFFFFF;
			p->state = state;
			break;
		case ZOMBIE:
			(p->pstime).ttime = ticks;
			p->state = state;
			break;
		default:
			panic("process state unrecognized");
			break;
	}
}

static void
updateStat1(void)
{
  struct proc *p;
  struct statetime *pstime;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
  {
	  pstime = &(p->pstime);
	  switch (p->state)
	  {
	  	  case UNUSED:
	  		  break;
	  	  case EMBRYO:
	  		  break;
	  	  case SLEEPING:
	  		  pstime->stime++;
	  		  break;
	  	  case RUNNABLE:
	  		  pstime->retime++;
	  		  break;
	  	  case RUNNING:
			  pstime->cfs_rutime += p->priority;
			  pstime->rutime++;
	  		  break;
	  	  case ZOMBIE:
	  		  break;
	  	  default:
	  		  panic("Unrecognized process state");
	  		  break;
	  }
  }
}

void
updateStat(void)
{
  acquire(&ptable.lock);
  updateStat1();
  release(&ptable.lock);
}

//PAGEBREAK: 32
// Look in the process table for an UNUSED proc.
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
	static int nextpid = 1;
	struct proc *p;
	char *sp;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == UNUSED)
      goto found;
  release(&ptable.lock);
  return 0;

found:
	updateState(p, EMBRYO);
	p->priority = MEDIUM;
	p->pid = nextpid++;
	release(&ptable.lock);

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
	  updateState(p, UNUSED);
	  return 0;
  }
  sp = p->kstack + KSTACKSIZE;
  
  // Leave room for trap frame.
  sp -= sizeof *p->tf;
  p->tf = (struct trapframe*)sp;
  
  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
  p->context->eip = (uint)forkret;
  return p;
}

//PAGEBREAK: 32
// Set up first user process.
void
userinit(void)
{
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];
  
  p = allocproc();
  initproc = p;
  if((p->pgdir = setupkvm()) == 0)
    panic("userinit: out of memory?");
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
  p->sz = PGSIZE;
  memset(p->tf, 0, sizeof(*p->tf));
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
  p->tf->es = p->tf->ds;
  p->tf->ss = p->tf->ds;
  p->tf->eflags = FL_IF;
  p->tf->esp = PGSIZE;
  p->tf->eip = 0;  // beginning of initcode.S

  safestrcpy(p->name, "initcode", sizeof(p->name));
  p->cwd = namei("/");

  updateState(p, RUNNABLE);
}

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
  uint sz;
  
  sz = proc->sz;
  if(n > 0){
    if((sz = allocuvm(proc->pgdir, sz, sz + n)) == 0)
      return -1;
  } else if(n < 0){
    if((sz = deallocuvm(proc->pgdir, sz, sz + n)) == 0)
      return -1;
  }
  proc->sz = sz;
  switchuvm(proc);
  return 0;
}

// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
fork(void)
{
  int i, pid;
  struct proc *np;

  // Allocate process.
  if((np = allocproc()) == 0)
    return -1;

  // Copy process state from p.
  if((np->pgdir = copyuvm(proc->pgdir, proc->sz)) == 0){
    kfree(np->kstack);
    np->kstack = 0;
    updateState(np, UNUSED);
    return -1;
  }
  np->sz = proc->sz;
  np->parent = proc;
  *np->tf = *proc->tf;

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
    if(proc->ofile[i])
      np->ofile[i] = filedup(proc->ofile[i]);
  np->cwd = idup(proc->cwd);

  safestrcpy(np->name, proc->name, sizeof(proc->name));
 
  pid = np->pid;

  // lock to force the compiler to emit the np->state write last.
  acquire(&ptable.lock);
  updateState(np, RUNNABLE);
  release(&ptable.lock);
  
  return pid;
}

// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait(0) to find out it exited.
void
exit(int status)
{
  struct proc *p;
  int fd;

  if(proc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd]){
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }

  begin_op();
  iput(proc->cwd);
  end_op();
  proc->cwd = 0;

  acquire(&ptable.lock);

  // Parent might be sleeping in wait(0).
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->parent == proc){
      p->parent = initproc;
      if(p->state == ZOMBIE)
    	  wakeup1(initproc);
    }
  }

  // Jump into the scheduler, never to return.
  updateState(proc, ZOMBIE);
  //*debug*
  //cprintf("proc %d dead. started %d ended %d runtime %d\nsleep: %d. ready: %d. running: %d.\n",
  //        proc->pid,proc->ctime,proc->ttime, check_time()-proc->ctime,proc->stime,proc->retime,proc->rutime);
  proc->exit_status = status;
  sched();
  panic("zombie exit");
}

int wait_stat(int *wtime, int *rtime, int *iotime, int *status)
{
  struct proc *p;
  struct statetime *pstime;
  int havekids, pid;

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != proc)
        continue;
      havekids = 1;
      if(p->state == ZOMBIE)
      {
    	  // Found one.
    	  pid = p->pid;
    	  pstime = &(p->pstime);

		  if(wtime)
			  *wtime = pstime->retime;
		  if(rtime)
			  *rtime = pstime->rutime;
		  if(iotime)
			  *iotime = pstime->stime;
  
		  kfree(p->kstack);
		  p->kstack = 0;
		  freevm(p->pgdir);
		  updateState(p, UNUSED);
		  p->pid = 0;
		  p->parent = 0;
		  p->name[0] = 0;
		  p->killed = 0;
		  if (status)
			*status = p->exit_status;

		  release(&ptable.lock);
		  return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
      release(&ptable.lock);
      return -1;
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
  }
  return -1;	// will never reach here.
}

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int
wait(int* status)
{
  struct proc *p;
  int havekids, pid;

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != proc)
        continue;
      havekids = 1;
      if(p->state == ZOMBIE){
        // Found one.
        pid = p->pid;
        kfree(p->kstack);
        p->kstack = 0;
        freevm(p->pgdir);
        updateState(p, UNUSED);
        p->pid = 0;
        p->parent = 0;
		p->name[0] = 0;
		p->killed = 0;
        if (status)
          *status = p->exit_status;
        release(&ptable.lock);
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
      release(&ptable.lock);
      return -1;
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
  }
  return -1;	// will never reach here.
}

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int
isalive(int pid)
{
	struct proc *p;
	int ret = 0;

	acquire(&ptable.lock);
  	// Scan through table looking for the proc with the pid
	for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
		if(p->pid == pid)    	  // found the damn pid
		{
			if (p->state == RUNNABLE || p->state == RUNNING || p->state == SLEEPING)
				ret = 1;
			break;
		}

	release(&ptable.lock);
	return ret;
}


// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int
waitpid(int pid, int* status, int options)
{
	struct proc *p = 0;
	int found = 0;

	acquire(&ptable.lock);
	for(;;)
	{	// if not found we look for the process with the pid
		if (!found)
			for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
				if (p->pid == pid)
				{
					found = 1;
					break;
				}

		// No point waiting if not found or killed or NONBLOCKING
		if(!found || proc->killed || options == NONBLOCKING)
		{
			release(&ptable.lock);
			return -1;
		}

		/*** if we got here, the pid was found: ***/

		// checking again if p->pid == pid because after the first sleep, the process may have initialized
		if (p && p->pid == pid)
		{
			if (p->state == ZOMBIE)
			{
				if (status)
					*status = p->exit_status;
				release(&ptable.lock);
				return pid;
			}
			else
				sleep(p->parent, &ptable.lock);
		}
		else
		{
			panic("The process was changed without been a ZOMBIE");
			return -1;
		}
	}
	return -1;	// will never reach here.
}

void switchToProcess(struct proc *p)
{
	// Switch to chosen process.  It is the process's job
	// to release ptable.lock and then reacquire it
	// before jumping back to us.

	proc = p;
	switchuvm(p);
    updateState(p, RUNNING);
	swtch(&cpu->scheduler, proc->context);
	switchkvm();

	// Process is done running for now.
	// It should have changed its p->state before coming back.
	proc = 0;
}

//PAGEBREAK: 42
// Per-CPU process scheduler.
// Each CPU calls scheduler() after setting itself up.
// Scheduler never returns.  It loops, doing:
//  - choose a process to run
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch back to the scheduler.
void
scheduler(void)
{
	struct proc *p, *procToRun;
	uint minimum;

	for(;;)
	{
		// Enable interrupts on this processor.
		sti();
		minimum = 0xFFFFFFFF; 	// maximum unsigned int
		procToRun = 0;
		p = 0;
		// Loop over process table looking for process to run.
		acquire(&ptable.lock);
		switch (SCHEDFLAG)
		{
			case DEFAULT:
				for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
					if(p->state == RUNNABLE)
						switchToProcess(p);
				break;
			case FRR:
			case FCFS:
				// Runs over all the ptable to find a runnable process
				// that got this state the first (begin_retime).
				for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
					if(p->state == RUNNABLE && (p->pstime).begin_retime < minimum)
					{
						minimum = (p->pstime).begin_retime;
						procToRun = p;
					}
				if (procToRun)	// if we did found a process
					switchToProcess(procToRun);
				break;
			case CFS:
				for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
					if(p->state == RUNNABLE && (p->pstime).cfs_rutime <= minimum)
					{
						if ((p->pstime).cfs_rutime == minimum)
						{// will get here only after procToRun was assign a process, but we check another time
							if (procToRun && (p->pstime).begin_retime < (procToRun->pstime).begin_retime)
								procToRun = p;
						}
						else
						{
							minimum = (p->pstime).cfs_rutime;
							procToRun = p;
						}
					}
				if (procToRun)	// if we did found a process
					switchToProcess(procToRun);
				break;
			default:
				panic("SCHEDFLAG was now found");
				break;
		}
		release(&ptable.lock);
	}
}


// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state.
void
sched(void)
{
  int intena;

  if(!holding(&ptable.lock))
    panic("sched ptable.lock");
  if(cpu->ncli != 1)
    panic("sched locks");
  if(proc->state == RUNNING)
    panic("sched running");
  if(readeflags()&FL_IF)
    panic("sched interruptible");
  intena = cpu->intena;
  swtch(&proc->context, cpu->scheduler);
  cpu->intena = intena;
}

// Give up the CPU for one scheduling round.
void
yield(void)
{
	acquire(&ptable.lock);  //DOC: yieldlock
	updateState(proc, RUNNABLE);
    sched();
    release(&ptable.lock);
}

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);

  if (first) {
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot 
    // be run from main().
    first = 0;
    initlog();
  }
  
  // Return to "caller", actually trapret (see allocproc).
}

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
void
sleep(void *chan, struct spinlock *lk)
{
  if(proc == 0)
    panic("sleep");

  if(lk == 0)
    panic("sleep without lk");

  // Must acquire ptable.lock in order to
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  if(lk != &ptable.lock){  //DOC: sleeplock0
    acquire(&ptable.lock);  //DOC: sleeplock1
    release(lk);
  }
  // Go to sleep.
  proc->chan = chan;
  updateState(proc, SLEEPING);
  sched();

  // Tidy up.
  proc->chan = 0;

  // Reacquire original lock.
  if(lk != &ptable.lock){  //DOC: sleeplock2
    release(&ptable.lock);
    acquire(lk);
  }
}

//PAGEBREAK!
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == SLEEPING && p->chan == chan)
    {
        updateState(p, RUNNABLE);
    }
}

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
  acquire(&ptable.lock);
  wakeup1(chan);
  release(&ptable.lock);
}

int setpriority(int priority)
{
	int oldPriority;
	if (priority <= 0 || priority > 3)
		return -1;

	acquire(&ptable.lock);
	oldPriority = proc->priority;
	proc->priority = priority;
	release(&ptable.lock);
	return oldPriority;
}

// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->pid == pid){
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
          updateState(p, RUNNABLE);
      release(&ptable.lock);
      return 0;
    }
  }
  release(&ptable.lock);
  return -1;
}

//PAGEBREAK: 36
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
  static char *states[] = {
  [UNUSED]    "unused",
  [EMBRYO]    "embryo",
  [SLEEPING]  "sleep ",
  [RUNNABLE]  "runble",
  [RUNNING]   "run   ",
  [ZOMBIE]    "zombie"
  };
  int i;
  struct proc *p;
  char *state;
  uint pc[10];
  
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
      state = states[p->state];
    else
      state = "???";
    cprintf("%d %s %s", p->pid, state, p->name);
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
  }
}

