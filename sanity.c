/*
 * sanity.c
 *
 *  Created on: Apr 7, 2015
 *      Author: dima
 */
#include "types.h"
#include "user.h"

int
main(int argc, char *argv[])
{
	int i = 0,
		N = 5,
		time_cons = 30,
		uptime_start = 0;
	int child_exit_status = 0,
		child_wtime = 0,
		child_rtime = 0,
		child_iotime = 0 ,
		child_pid = 0;
	int avrg_wtime = 0,
		avrg_rtime = 0,
		avrg_iotime = 0;

	// change priority to HIGH
	setpriority(1);
	// fork N child processes
	for (i = 0; i < N; i++)
	{
		if (fork() == 0)
		{	// the child:
			// each child updates its priority according to its creation index modulo 3
			setpriority((i % 3) + 1);
			// time-consuming computation: 30 clock ticks
			uptime_start = uptime();
			while (uptime() - uptime_start < time_cons) {}
			// exit with my pid
			exit(getpid());
		}
	}

	// papa:
	for (i = 0; i < N; i++)
	{
		// papa waits to all N children
		child_pid = wait_stat(&child_wtime, &child_rtime, &child_iotime, &child_exit_status);
		// validate its exit status to there pid number
		if (child_pid == child_exit_status)
		{
			avrg_wtime += child_wtime;
			avrg_rtime += child_rtime;
			avrg_iotime += child_iotime;
			printf(1, "\n got pid: %d\n", child_pid);
			printf(1, "waiting time is: %d\n", child_wtime);
			printf(1, "running time is: %d\n", child_rtime);
			printf(1, "sleeping (waiting for io) time is: %d\n", child_iotime);
			printf(1, "\n averages:\n");
			printf(1, "average waiting time: %d\n", avrg_wtime/(i+1));
			printf(1, "average running time: %d\n", avrg_rtime/(i+1));
			printf(1, "average sleeping time: %d\n", avrg_iotime/(i+1));
		}
		else
		{
			// TODO: nobody knows.. again!
			printf(1, "oh oh, the child pid: %d and status: %d are n the same\n", child_pid, child_exit_status);
		}

	}
	exit(1);
}
